/**
 * @FileName			UserService.java
 * @Author				BOCAI
 * @Version				V1.0
 * @CreateTime		2017年4月22日		下午2:09:20
 * @Copyright			Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.system.user;



/**
 * @ClassName			UserService
 * @Description			用户信息的业务控制器
 * @Author					BOCAI
 * @CreateTime			2017年4月22日		下午2:09:20
 * @Version					V1.0
 * @History
 * Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年4月22日     	BOCAI      	1.0             1.0
 * 
 * Why & What is modified:
 * 
 */
public interface UserService {
	
	/**
	 * 
	 * 判断当前用户是否登录成功
	 * @param user		用户信息（用户名、密码）
	 * @return				成功返回true,否则返回false
	 * @throws Exception
	 */
	public boolean login(User user) throws Exception;
	
	
	/**
	 * 
	 * 注册新用户
	 * @param user		用户信息
	 * @return				成功返回true,否则返回false
	 * @throws Exception
	 */
	public boolean register(User user) throws Exception;
	

	/**
	 * 
	 * 获取完整用户信息
	 * @param user		用户信息
	 * @return				返回完整用户信息，否则返回null
	 * @throws Exception
	 */
	public User getUser(User user) throws Exception;
	
	
	/**
	 * 
	 * 修改用户信息
	 * @param user		用户信息
	 * @return				修改成功返回true，否则返回false
	 * @throws Exception
	 */
	public boolean updateUser(User user) throws Exception;
	

}
