/**
 * @FileName			UserServiceImp.java
 * @Author				BOCAI
 * @Version				V1.0
 * @CreateTime		2017年4月22日		下午7:14:28
 * @Copyright			Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.system.user;

import java.util.List;

import com.like.common.ResultType;
import com.like.common.util.IcoUtils;
import com.like.common.util.IdUtils;

/**
 * @ClassName			UserServiceImp
 * @Description			用户信息的业务控制器
 * @Author					BOCAI
 * @CreateTime			2017年4月22日		下午7:14:28
 * @Version					V1.0
 * @History
 * Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年4月22日     	BOCAI      	1.0             1.0
 * 
 * Why & What is modified:
 * 
 */
public class UserServiceImp implements UserService {
	
	private UserDao dao;
	
	public UserServiceImp(){
		this.dao =  new UserDaoImp();
	}


	@Override
	public boolean login(User user) throws Exception {
		boolean result = false;
		
		if( !userisNull(user, 1)){
			return result;
		}
		
		int rowCount = this.dao.size(user);
		result = rowCount == 1 ? true : false;
		return result;
	}

	@Override
	public boolean register(User user) throws Exception {
		boolean result = false;
		
		if( !userisNull(user, 2)){
			return result;
		}
		
		User u = new User();
		u.setUlogon(user.getUlogon());
		int size = this.dao.size(u);
		
		if(size == 0){
			user.setUid(IdUtils.getUUID());
			user.setUsex(UserArgument.USER_DEFAULT_SEX);
			user.setUinfo(UserArgument.USER_DEFAULT_INFO);
			user.setUimage(IcoUtils.icoCreate());
			int type = this.dao.add(user);
			result = ResultType.get(type);
		}
		
		return result;
	}

	@Override
	public User getUser(User user) throws Exception {
		User result = new User();
		List<User> list = this.dao.list(user);
		result = list.get(0);
		return result;
	}


	@Override
	public boolean updateUser(User user) throws Exception {
		boolean result = false;
		int type = this.dao.update(user);
		result = ResultType.get(type);
		return result;
	}

	/**
	 * 
	 * @Description		判断输入信息是否为空
	 * @param user		用户实体
	 * @param type		1表示验证账号密码，2表示验证账号密码呢名
	 * @return
	 */
	private boolean userisNull(User user,int type){
		boolean result = false;
		
		if( type == 1 || type == 2){
			
			if( user.getUlogon() == null || user.getUlogon().equals("")){
				return result;
			}
			
			if( user.getUpasswd() == null || user.getUpasswd().equals("")){
				return result;
			}
			
		}
		
		if(type == 2){
			
			if( user.getUalais()== null || user.getUalais().equals("")){
				return result;
			}
			
		}
		
		result = true;
		return result;
	}

	

}
