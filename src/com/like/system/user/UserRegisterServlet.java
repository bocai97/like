/**
 * @FileName			UserRegisterServlet.java
 * @Author				BOCAI
 * @Version				V1.0
 * @CreateTime		2017年5月22日		上午10:37:40
 * @Copyright			Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.system.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.like.common.JsonResult;
import com.like.common.ResultType;

import net.sf.json.JSONObject;


/**
 * @ClassName			UserRegisterServlet
 * @Description			用户注册的视图控制器
 * @Author					BOCAI
 * @CreateTime			2017年5月22日		上午10:37:40
 * @Version					V1.0
 * @History
 * Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年5月22日     	BOCAI      	1.0             1.0
 * 
 * Why & What is modified:
 * 
 */
@SuppressWarnings("serial")
public class UserRegisterServlet extends HttpServlet {
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		User user = new User();
		UserService service = new UserServiceImp();
		JsonResult jsonresult = new JsonResult();
		user.setUalais(request.getParameter("ualais"));
		user.setUlogon(request.getParameter("ulogon"));
		user.setUpasswd(request.getParameter("upasswd"));
		
		try{
			
			if(service.register(user)){
				jsonresult.setStatus(ResultType.SUCCESS);
				jsonresult.setMsg(UserArgument.REGISTER_SUCCESS_MEG);
				
			}else{
				jsonresult.setStatus(ResultType.FAIL);
				jsonresult.setMsg(UserArgument.REGISTER_FAIL_MEG);
				
			}
			
		response.getWriter().println(JSONObject.fromObject(jsonresult).toString());

		}catch(Exception e){
			
			getServletContext().log(e.toString());
			
		}
		
	}

}
