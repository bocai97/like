/**
 * @FileName			UserDaoImp.java
 * @Author				BOCAI
 * @Version				V1.0
 * @CreateTime		2017年4月22日		下午7:13:56
 * @Copyright			Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.system.user;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;

import com.like.common.ResultType;
import com.like.common.jdbc.DaoSupport;
import com.like.common.util.StringUtils;



/**
 * @ClassName			UserDaoImp
 * @Description			用户信息的数据持久控制器
 * @Author					BOCAI
 * @CreateTime			2017年4月22日		下午7:13:56
 * @Version					V1.0
 * @History
 * Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年4月22日     	BOCAI      	1.0             1.0
 * 
 * Why & What is modified:
 * 
 */
public class UserDaoImp extends DaoSupport implements UserDao {
	
	public UserDaoImp(){
		this.logger = Logger.getLogger(UserDaoImp.class);
	}


	@Override
	public int add(User user) throws Exception {
		int result = ResultType.FAIL;
		
		try{
			this.setConnection(true);
			String sql = UserArgument.INSERT_SQL ;
			statement = this.connection.prepareStatement(sql);
			List<String> list = StringUtils.getFieldBySql(sql);
			int location = 1;
			
			for(String field : list){
				statement.setString(location, BeanUtils.getProperty(user, field));
				location++;	
			}	
			
			result = statement.executeUpdate();
			
		}catch(Exception e){
			this.logger.error(e);
			throw e;
			
		}finally{
			this.dispose(this.connection, this.statement, this.set);
		}
		
		return result;
	}


	@Override
	public int delete(User user) throws Exception {
		int result = ResultType.FAIL;
		
		try{
			this.setConnection(true);
			String sql = UserArgument.DELETE_SQL;
			statement = this.connection.prepareStatement(sql);
			statement.setString(1, BeanUtils.getProperty(user, "uid"));
			result = statement.executeUpdate();
			
		}catch(Exception e){
			this.logger.error(e);
			throw e;
			
		}finally{
			this.dispose(this.connection, this.statement, this.set);
			
		}
		
		return result;
	}


	@Override
	public int update(User user) throws Exception {
		int result = ResultType.FAIL;
		
		try{
			this.setConnection(true);
			String sql = UserArgument.UPDATE_SQL;
			String[] Strings =  this.setWhere(user,1);
			sql = sql + Strings[2];
			statement = this.connection.prepareStatement(sql);

			if(Strings[1].length() != 0){
				Strings = Strings[1].split(",");
				int location = 1;
				
				for(String field : Strings){
					statement.setString(location, BeanUtils.getProperty(user, field));
					location++;
					
				}
				
			}
			
			result = statement.executeUpdate();
			
		}catch(Exception e){
			this.logger.error(e);
			throw e;
			
		}finally{
			this.dispose(this.connection, this.statement, this.set);
			
		}
		
		return result;
	}


	@Override
	public List<User> list(User user) throws Exception {
		return this.list(user, 0, -1);
	}


	@Override
	public List<User> list(User user, int start, int count) throws Exception {
		return this.list(user, start, count, "uid");
	}


	@Override
	public List<User> list(User user, int start, int count, String orderBy) throws Exception {
		List<User> result = new ArrayList<User>();
		
		try{
			this.setConnection(true);
			int rowCount = this.size(user,this.connection);
			String sql = UserArgument.QUERY_SQL;
			String[] Strings =  this.setWhere(user,0);
			sql = sql + Strings[0];
			
			if( orderBy != null && !orderBy.equals("") ){
				sql = sql + " order by " + orderBy ;
			}
			
			if( count > 0 ){
				
				if( ( start + count ) >= rowCount ){
					count = count - ( start + count - rowCount );
				}
				
				sql = sql + " limit " + start +  "," + count;
			}
			
			this.statement = this.connection.prepareStatement(sql);
			
			if(Strings[1].length() != 0){
				Strings = Strings[1].split(",");
				int location = 1;
				
				for(String field : Strings){
					statement.setString(location, BeanUtils.getProperty(user, field));
					location++;
				}
				
			}
			
			this.set = this.statement.executeQuery();
			
			while( this.set.next() ){
				User obj = new User();
				BeanUtils.setProperty(obj, "uid", this.set.getString("uid"));
				BeanUtils.setProperty(obj, "ualais", this.set.getString("ualais"));
				BeanUtils.setProperty(obj, "ulogon", this.set.getString("ulogon"));
				BeanUtils.setProperty(obj, "upasswd", this.set.getString("upasswd"));
				BeanUtils.setProperty(obj, "usex", this.set.getString("usex"));
				BeanUtils.setProperty(obj, "uinfo", this.set.getString("uinfo"));
				BeanUtils.setProperty(obj, "uimage", this.set.getString("uimage"));
				result.add(obj);
			}
			
		}catch(Exception e ){
			this.logger.error(e);
			throw e;
			
		}finally{
			this.dispose(connection, statement, set);
			
		}
		
		return result;
	}


	@Override
	public int size(User user) throws Exception {
		int rowCount = 0 ;
		
		try{
			this.setConnection(true);
			rowCount = this.size(user, connection);
			
		}catch(Exception e ){
			this.logger.error(e);
			throw e;
			
		}finally{
			this.dispose(connection, statement, set);
			
		}
		
		return rowCount ;
	}

	@Override
	public User get(String id) throws Exception {
		User result = null ;
		
		try{
			this.setConnection(true);
			String sql = UserArgument.QUERY_SQL ;
			sql = sql + " and uid=? ";
			this.statement = this.connection.prepareStatement(sql);
			this.statement.setString(1, id);
			this.set = this.statement.executeQuery();
			result = new User();
			
			if( this.set.next() ){
				BeanUtils.setProperty(result, "uid", this.set.getString("uid"));
				BeanUtils.setProperty(result, "ualais", this.set.getString("ualais"));
				BeanUtils.setProperty(result, "ulogon", this.set.getString("ulogon"));
				BeanUtils.setProperty(result, "upasswd", this.set.getString("upasswd"));
				BeanUtils.setProperty(result, "usex", this.set.getString("usex"));
				BeanUtils.setProperty(result, "uinfo", this.set.getString("uinfo"));
				BeanUtils.setProperty(result, "uimage", this.set.getString("uimage"));
			}
			
		}catch(Exception e ){
			this.logger.error(e);
			throw e;
			
		}finally{
			this.dispose(connection, statement, set);
			
		}
		
		return result ;
	}
	
	/**
	 * 
	 * @Description				根据过滤条件获取统计量
	 * @param user				保存过滤条件的信息实体
	 * @param connection		连接对象
	 * @return						返回统计量
	 * @throws Exception
	 */
	private int size(User user,Connection connection) throws Exception {
		int result = 0 ;
		PreparedStatement statement = null ;
		ResultSet set = null ;
		
		try{
			String sql = UserArgument.COUNT_SQL ;	
			String[] Strings =  this.setWhere(user , 0);
			sql = sql + Strings[0];
			statement = connection.prepareStatement(sql);

			if(Strings[1].length() != 0){
				Strings = Strings[1].split(",");
				int location = 1;
				
				for(String field : Strings){
					statement.setString(location, BeanUtils.getProperty(user, field));
					location++;
				}
				
			}
		
			set = statement.executeQuery();
			
			if( set.next() ){
				result = Integer.valueOf( set.getString(1) );
			}
			
		}catch(Exception e){
			this.logger.error(e);
			throw e;
			
		}finally{
			this.dispose(null,statement,set);
			
		}

		return result;
	}
	
	
	/**
	 * 
	 * @Description		拼接过滤条件，统计占位符字段
	 * @param user		占位符的值
	 * @param type		0代表查询，1代表更新
	 * @return				返回过滤条件和占位符字段 
	 * 								数组0表示查询过滤条件
	 * 								数组1表示统计占位符字段
	 * 								数组2表示更新过滤条件
	 * @throws Exception
	 */
	private String[] setWhere(User user, int type) throws Exception{
		StringBuffer where = new StringBuffer();
		StringBuffer field = new StringBuffer();
		String string = new String();
		
		if( user.getUalais()!= null && !user.getUalais().equals("") ){
			
			if(type == 1){
				string = string + " ualais=?,";
			}else{
				where.append(" and ualais=? ");
			}
			
			field.append("ualais,");
		}
			
		if( user.getUlogon()!= null && !user.getUlogon().equals("") ){
			
			if(type == 1){
				string = string + " ulogon=?,";
			}else{
				where.append(" and ulogon=? ");
			}
			
			field.append("ulogon,");
		}
			
		if( user.getUpasswd()!= null && !user.getUpasswd().equals("") ){
			
			if(type == 1){
				string = string + " upasswd=?,";
			}else{
				where.append(" and upasswd=? ");
			}
			field.append("upasswd,");
		}
			
		if( user.getUsex()!= null && !user.getUsex().equals("") ){
			
			if(type == 1){
				string = string + " usex=?,";	
			}else{
				where.append(" and usex=? ");	
			}
			
			field.append("usex,");
		}
		
		if( user.getUinfo()!= null && !user.getUinfo().equals("") ){
			
			if(type == 1){
				string = string + " uinfo=?,";	
			}else{
				where.append(" and uinfo=? ");	
			}
			
			field.append("uinfo,");
		}
			
		if( user.getUimage()!= null && !user.getUimage().equals("") ){
			
			if(type == 1){
				string = string + " uimage=?,";	
			}else{
				where.append(" and uimage=? ");	
			}
			field.append("uimage,");
		}
		
		if(type == 1 && string.length() != 0){
			string = string.substring(0, string.length() - 1);
		}
			
		if( user.getUid()!= null && !user.getUid().equals("") ){
			
			if(type == 1){
				string = string + " where uid=? ";
			}else{
				where.append(" and uid=? ");
			}

			field.append("uid,");		
		}
		
		String[] Strings = new String[]{where.toString(),field.toString(),string};
		return Strings;
	}
	
}
