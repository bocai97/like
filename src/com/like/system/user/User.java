/**
 * @FileName			User.java
 * @Author				BOCAI
 * @Version				V1.0
 * @CreateTime		2017年4月22日		下午2:39:58
 * @Copyright			Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.system.user;

/**
 * @ClassName			User
 * @Description			用户信息的实体
 * @Author					BOCAI
 * @CreateTime			2017年4月22日		下午2:39:58
 * @Version					V1.0
 * @History
 * Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年4月22日     	BOCAI      	1.0             1.0
 * 
 * Why & What is modified:
 * 
 */
public class User {
	
	/**
	 * 用户编号
	 */
	private String uid;
	
	/**
	 * 用户昵称
	 */
	private String ualais;
	
	/**
	 * 用户账号
	 */
	private String ulogon;
	
	/**
	 * 用户密码
	 */
	private String upasswd;
	
	/**
	 * 用户性别
	 */
	private String usex;
	
	/**
	 * 用户简介
	 */
	private String uinfo;
	
	/**
	 * 用户头像
	 */
	private String uimage;
	
	
	public User(){
		super();
	}

	/**
	 * @return 用户编号
	 */
	public String getUid() {
		return uid;
	}


	/**
	 * @param 用户编号
	 */
	public void setUid(String uid) {
		this.uid = uid;
	}


	/**
	 * @return 用户昵称
	 */
	public String getUalais() {
		return ualais;
	}


	/**
	 * @param 用户昵称
	 */
	public void setUalais(String ualais) {
		this.ualais = ualais;
	}


	/**
	 * @return 用户账号
	 */
	public String getUlogon() {
		return ulogon;
	}


	/**
	 * @param 用户账号
	 */
	public void setUlogon(String ulogon) {
		this.ulogon = ulogon;
	}


	/**
	 * @return 用户密码
	 */
	public String getUpasswd() {
		return upasswd;
	}


	/**
	 * @param 用户密码
	 */
	public void setUpasswd(String upasswd) {
		this.upasswd = upasswd;
	}


	/**
	 * @return 用户性别
	 */
	public String getUsex() {
		return usex;
	}


	/**
	 * @param 用户性别
	 */
	public void setUsex(String usex) {
		this.usex = usex;
	}


	/**
	 * @return 用户简介
	 */
	public String getUinfo() {
		return uinfo;
	}


	/**
	 * @param 用户简介
	 */
	public void setUinfo(String uinfo) {
		this.uinfo = uinfo;
	}


	/**
	 * @return 用户头像
	 */
	public String getUimage() {
		return uimage;
	}


	/**
	 * @param 用户头像
	 */
	public void setUimage(String uimage) {
		this.uimage = uimage;
	}
	
	
	
}
