/**
 * @FileName			UserArgument.java
 * @Author				BOCAI
 * @Version				V1.0
 * @CreateTime		2017年4月22日		下午2:01:04
 * @Copyright			Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.system.user;

/**
 * @ClassName			UserArgument
 * @Description			用户信息的常量类
 * @Author					BOCAI
 * @CreateTime			2017年4月22日		下午2:01:04
 * @Version					V1.0
 * @History
 * Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年4月22日     	BOCAI      	1.0             1.0
 * 
 * Why & What is modified:
 * 
 */
public class UserArgument {
	
	/**
	 * 插入记录sql语句
	 */
	public static final String INSERT_SQL = "insert into user(uid,ualais,ulogon,upasswd,usex,uinfo,uimage)values(?,?,?,?,?,?,?)";
	
	/**
	 * 删除记录sql语句
	 */
	public static final String DELETE_SQL = "delete from user where uid = ?";
	
	/**
	 * 修改记录sql语句
	 */
	public static final String UPDATE_SQL = "update user set";
	
	/**
	 * 查询记录sql语句
	 */
	public static final String QUERY_SQL = "select uid,ualais,ulogon,upasswd,usex,uinfo,uimage from user where 1=1";
	
	/**
	 * 统计记录sql语句
	 */
	public static final String COUNT_SQL = "select count(uid) from user where 1=1 ";
	
	/**
	 * 用户性别默认值
	 */
	public static final String USER_DEFAULT_SEX = "保密";
	
	/**
	 * 用户简介默认值
	 */
	public static final String USER_DEFAULT_INFO = "这个用户很懒，暂时没有写简介";
	
	/**
	 * 用户头像默认值
	 */
	public static final String USER_DEFAULT_IMAGE = "ico.png";
	
	/**
	 * 用户登录成功提示信息
	 */
	public static final String LOGIN_SUCCESS_MEG = "登录成功";
	
	/**
	 * 用户登录失败提示信息
	 */
	public static final String LOGIN_FAIL_MEG = "登录失败，账号或密码错误";
	
	/**
	 * 用户登录成功跳转的路径
	 */
	public static final String LOGIN_SUCCESS_URL = "../message/list.do";
	
	/**
	 * 用户信息详情jsp页面的路径
	 */
	public static final String USER_INFO_JSP = "./info.jsp";
	
	/**
	 * 用户推出登录后跳转的页面路径
	 */
	public static final String USER_EXIT = "/user/login.jsp";
	
	
	/**
	 * 用户注册成功提示信息
	 */
	public static final String REGISTER_SUCCESS_MEG = "注册成功";
	
	/**
	 * 用户注册失败提示信息
	 */
	public static final String REGISTER_FAIL_MEG = "注册失败，账号已存在";
	
}
