/**
 * @FileName			UserUpdateServlet.java
 * @Author				BOCAI
 * @Version				V1.0
 * @CreateTime		2017年5月22日		上午10:46:38
 * @Copyright			Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.system.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.like.common.ContextArgument;
import com.like.common.JsonResult;
import com.like.common.ResultType;

import net.sf.json.JSONObject;

/**
 * @ClassName			UserUpdateServlet
 * @Description			更新用户信息的视图控制器
 * @Author					BOCAI
 * @CreateTime			2017年5月22日		上午10:46:38
 * @Version					V1.0
 * @History
 * Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年5月22日     	BOCAI      	1.0             1.0
 * 
 * Why & What is modified:
 * 
 */
@SuppressWarnings("serial")
public class UserUpdateServlet extends HttpServlet {
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		User user = new User();
		UserService service = new UserServiceImp();
		JsonResult jsonresult = new JsonResult();
		HttpSession session = request.getSession();
		user.setUid((String)session.getAttribute(ContextArgument.UID));
		user.setUalais(request.getParameter("ualais"));
		user.setUsex(request.getParameter("usex"));
		user.setUinfo(request.getParameter("uinfo"));
		try{
			
			if(service.updateUser(user)){
				session.setAttribute(ContextArgument.UALAIS, user.getUalais());
				jsonresult.setStatus(ResultType.SUCCESS);
				jsonresult.setMsg(ContextArgument.SUCCESS);
				
			}else{
				jsonresult.setStatus(ResultType.FAIL);
				jsonresult.setMsg(ContextArgument.FAIL);
				
			}
			
		response.getWriter().println(JSONObject.fromObject(jsonresult).toString());

		}catch(Exception e){
			
			getServletContext().log(e.toString());
			
		}
		
	}

}
