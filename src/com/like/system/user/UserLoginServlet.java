/**
 * @FileName			UserLoginServlet.java
 * @Author				BOCAI
 * @Version				V1.0
 * @CreateTime		2017年5月21日		上午1:44:08
 * @Copyright			Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.system.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.like.common.ContextArgument;
import com.like.common.JsonResult;
import com.like.common.ResultType;

import net.sf.json.JSONObject;

/**
 * @ClassName			UserLoginServlet
 * @Description			用户登录的视图控制器
 * @Author					BOCAI
 * @CreateTime			2017年5月21日		上午1:44:08
 * @Version					V1.0
 * @History
 * Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年5月21日     	BOCAI      	1.0             1.0
 * 
 * Why & What is modified:
 * 
 */
@SuppressWarnings("serial")
public class UserLoginServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		User user = new User();
		UserService service = new UserServiceImp();
		JsonResult jsonresult = new JsonResult();
		user.setUlogon(request.getParameter("ulogon"));
		user.setUpasswd(request.getParameter("upasswd"));
		
		try{
			if(service.login(user)){
				user = service.getUser(user);
				HttpSession session = request.getSession();
				session.setAttribute(ContextArgument.UID, user.getUid());
				session.setAttribute(ContextArgument.UALAIS, user.getUalais());
				jsonresult.setStatus(ResultType.SUCCESS);
				jsonresult.setMsg(UserArgument.LOGIN_SUCCESS_MEG);
				jsonresult.setJsonData(UserArgument.LOGIN_SUCCESS_URL);
				
			}else{
				jsonresult.setStatus(ResultType.FAIL);
				jsonresult.setMsg(UserArgument.LOGIN_FAIL_MEG);

			}
			
		response.getWriter().println(JSONObject.fromObject(jsonresult).toString());
			
		}catch(Exception e){
			
			getServletContext().log(e.toString());
			
		}
		
	}
	
	

}
