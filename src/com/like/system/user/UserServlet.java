/**
 * @FileName			Userservlet.java
 * @Author				BOCAI
 * @Version				V1.0
 * @CreateTime		2017年4月22日		下午2:11:42
 * @Copyright			Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.system.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.like.common.ContextArgument;

/**
 * @ClassName			Userservlet
 * @Description			用户信息的视图控制器
 * @Author					BOCAI
 * @CreateTime			2017年4月22日		下午2:11:42
 * @Version					V1.0
 * @History
 * Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年4月22日     	BOCAI      	1.0             1.0
 * 
 * Why & What is modified:
 * 
 */
@SuppressWarnings("serial")
public class UserServlet extends HttpServlet {
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		User user = new User();
		UserService service = new UserServiceImp();
		HttpSession session = request.getSession();
		user.setUid((String)session.getAttribute(ContextArgument.UID));

		try{	
			user = service.getUser(user);
			request.setAttribute(ContextArgument.USER, user);
			request.getRequestDispatcher(UserArgument.USER_INFO_JSP).forward(request, response);
			
		}catch(Exception e){
			
			getServletContext().log(e.toString());
			
		}
		
	}

}
