/**
 * @FileName			ReplyDao.java
 * @Author				BOCAI
 * @Version				V1.0
 * @CreateTime		2017年4月22日		下午2:14:45
 * @Copyright			Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.system.reply;

import java.util.List;

/**
 * @ClassName			ReplyDao
 * @Description			回复消息的数据持久控制器
 * @Author					BOCAI
 * @CreateTime			2017年4月22日		下午2:14:45
 * @Version					V1.0
 * @History
 * Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年4月22日     	BOCAI      	1.0             1.0
 * 
 * Why & What is modified:
 * 
 */
public interface ReplyDao {
	
	/**
	 * 
	 * 新增当前回复消息
	 * @param reply		回复消息实体
	 * @return				成功返回 >0 的数字，否则返回 <=0 的数字
	 * @throws Exception
	 */
	public int add( Reply reply ) throws Exception;
	
	/**
	 * 
	 * 删除当前回复消息
	 * @param reply		回复消息实体
	 * @return				成功返回 >0 的数字，否则返回 <=0 的数字
	 * @throws Exception
	 */
	public int delete( Reply reply ) throws Exception;
	
	/**
	 * 
	 * 修改当前回复消息
	 * @param reply		回复消息实体
	 * @return				成功返回 >0 的数字，否则返回 <=0 的数字
	 * @throws Exception
	 */
	public int update( Reply reply ) throws Exception;
		
	/**
	 * 
	 * 根据查询条件获取所有回复消息
	 * @param reply		保存过滤条件的回复消息
	 * @return				返回List<Reply>类型对象，否则返回null
	 * @throws Exception
	 */
	public List<Reply> list( Reply reply ) throws Exception;	
	
	/**
	 * 
	 * 根据查询条件获取所有回复消息
	 * @param reply		保存过滤条件的回复消息
	 * @param start		分页条件的起始行号
	 * @param count		分页条件的单页记录数
	 * @return				返回List<Reply>类型对象，否则返回null
	 * @throws Exception
	 */
	public List<Reply> list( Reply reply , int start , int count )throws Exception;

	/**
	 * 
	 * 根据查询条件获取所有回复消息
	 * @param reply		保存过滤条件的回复消息
	 * @param start		分页条件的起始行号
	 * @param count		分页条件的单页记录数
	 * @param orderBy	排序条件
	 * @return				返回List<Reply>类型对象，否则返回null
	 * @throws Exception
	 */
	public List<Reply> list( Reply reply , int start , int count , String orderBy ) throws Exception;
	
	/**
	 * 
	 * 根据过滤条件获取回复消息记录行的总量
	 * @param reply		过滤条件
	 * @return				返回总量，否则返回0
	 * @throws Exception
	 */
	public int size( Reply reply ) throws Exception;
	
	/**
	 * 
	 * 根据ID获取完整的回复消息
	 * @param id			回复消息编号
	 * @return				成功返回Reply类型对象，否则返回null
	 * @throws Exception
	 */
	public Reply get(String id) throws Exception ;

}
