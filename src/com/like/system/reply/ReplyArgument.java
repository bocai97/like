/**
 * @FileName			ReplyArgument.java
 * @Author				BOCAI
 * @Version				V1.0
 * @CreateTime		2017年4月22日		下午2:13:06
 * @Copyright			Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.system.reply;

/**
 * @ClassName			ReplyArgument
 * @Description			消息回复的常量类
 * @Author					BOCAI
 * @CreateTime			2017年4月22日		下午2:13:06
 * @Version					V1.0
 * @History
 * Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年4月22日     	BOCAI      	1.0             1.0
 * 
 * Why & What is modified:
 * 
 */
public class ReplyArgument {
	
	/**
	 * 插入记录sql语句
	 */
	public static final String INSERT_SQL = "insert into reply(rid,rcontent,rdatetime,mid,uid)values(?,?,?,?,?)";
	
	/**
	 * 删除记录sql语句
	 */
	public static final String DELETE_SQL = "delete from reply where rid = ?";
	
	/**
	 * 修改记录sql语句
	 */
	public static final String UPDATE_SQL = "update reply set";
	
	/**
	 * 查询记录sql语句
	 */
	public static final String QUERY_SQL =  "select user.uid,user.ualais,user.uimage,reply.rid,reply.rcontent,reply.rdatetime,reply.mid from user,reply where reply.uid = user.uid";
	
	/**
	 * 统计记录sql语句
	 */
	public static final String COUNT_SQL = "select count(rid) from reply where 1=1 ";
	
	/**
	 * 表示发表成功的提示信息
	 */
	public static final String ADD_SUCCESS = "发表成功" ;
	
	/**
	 * 表示发表失败的提示信息
	 */
	public static final String ADD_FAIL = "发表失败" ;
	
	/**
	 * 表示删除成功的提示信息
	 */
	public static final String DELETE_SUCCESS = "删除成功" ;
	
	/**
	 * 表示删除失败的提示信息
	 */
	public static final String DELETE_FAIL = "删除失败" ;
	
	/**
	 * 表示个人回复JSP页面路径
	 */
	public static final String MYREPLY_JSP = 	"../user/myreply.jsp" ;


}
