/**
 * @FileName			ReplyServiceImp.java
 * @Author				BOCAI
 * @Version				V1.0
 * @CreateTime		2017年4月22日		下午7:15:41
 * @Copyright			Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.system.reply;

import java.util.List;

import com.like.common.ResultType;
import com.like.common.util.IdUtils;



/**
 * @ClassName			ReplyServiceImp
 * @Description			消息回复的业务控制器
 * @Author					BOCAI
 * @CreateTime			2017年4月22日		下午7:15:41
 * @Version					V1.0
 * @History
 * Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年4月22日     	BOCAI      	1.0             1.0
 * 
 * Why & What is modified:
 * 
 */
public class ReplyServiceImp implements ReplyService {
	
	private ReplyDao dao;
	
	public ReplyServiceImp(){
		this.dao =  new ReplyDaoImp();
	}

	@Override
	public boolean addReply(Reply reply) throws Exception {
		boolean result = false;
		
		if( !replyisNull(reply, 1)){
			return result;
		}
		
		reply.setRid(IdUtils.getUUID());
		int type = this.dao.add(reply);
		result = ResultType.get(type);
	
		return result;
	}

	@Override
	public boolean deleteReply(Reply reply) throws Exception {		
		boolean result = false;
		
		if( !replyisNull(reply, 2)){
			return result;
		}
		
		int size = this.dao.size(reply);
		
		if(size != 0){
			int type = this.dao.delete(reply);
			result = ResultType.get(type);
		}
		
		return result;
	}

	@Override
	public List<Reply> getReplyList(Reply reply, int page) throws Exception {
		List<Reply> result = null;	
		int start = (page - 1) * 10;
		result = this.dao.list(reply, start, 10);
		return result;
	}

	@Override
	public int getReplySize(Reply reply) throws Exception {
		int result = 0;
		int size = this.dao.size(reply);
//		分页总量 = （总记录数 + 分页记录数-1）/分页记录数
		result = ( size + 10 - 1) / 10;
		return result;
	}
	

	/**
	 * 
	 * 判断输入信息是否为空
	 * @param reply		回复实体
	 * @param type		过滤状态，1表示添加回复，2表示删除回复
	 * @return
	 */
	private boolean replyisNull( Reply reply, int type){
		boolean result = false;
		if( type == 1){
			if( reply.getRcontent() == null || reply.getRcontent().equals("")){
				return result;
			}
			
			if( reply.getUid() == null || reply.getUid().equals("")){
				return result;
			}
			
			if( reply.getMid() == null || reply.getMid().equals("")){
				return result;
			}
		}

		if( type == 2){
			if( reply.getRid() == null || reply.getRid().equals("")){
				return result;
			}
		}
		
		result = true;
		return result;
	}
	
}
