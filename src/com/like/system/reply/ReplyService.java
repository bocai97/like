/**
 * @FileName			ReplyService.java
 * @Author				BOCAI
 * @Version				V1.0
 * @CreateTime		2017年4月22日		下午2:15:26
 * @Copyright			Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.system.reply;

import java.util.List;


/**
 * @ClassName			ReplyService
 * @Description			消息回复的业务控制器
 * @Author					BOCAI
 * @CreateTime			2017年4月22日		下午2:15:26
 * @Version					V1.0
 * @History
 * Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年4月22日     	BOCAI      	1.0             1.0
 * 
 * Why & What is modified:
 * 
 */
public interface ReplyService {
	
	/**
	 * 
	 * 添加回复
	 * @param reply				回复实体
	 * @return						成功返回true，否则返回false
	 * @throws Exception
	 */
	public boolean addReply(Reply reply) throws Exception;
	
	/**
	 * 
	 * 删除回复
	 * @param reply				回复实体
	 * @return						成功返回true，否则返回false
	 * @throws Exception
	 */
	public boolean deleteReply(Reply reply) throws Exception;
	
	/**
	 * 
	 * 获取回复列表
	 * @param reply			回复实体
	 * @return					返回List<Reply>类型对象，否则返回null
	 * @throws Exception
	 */
	public List<Reply> getReplyList(Reply reply, int page) throws Exception;
	
	/**
	 * 
	 * 获取回复总页数
	 * @param reply			回复实体
	 * @return
	 * @throws Exception	返回回复总页数，否则返回0
	 */
	public int getReplySize(Reply reply) throws Exception;

}
