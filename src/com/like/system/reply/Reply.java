/**
 * @FileName			Reply.java
 * @Author				BOCAI
 * @Version				V1.0
 * @CreateTime		2017年4月22日		下午2:50:17
 * @Copyright			Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.system.reply;

import com.like.system.user.User;

/**
 * @ClassName			Reply
 * @Description			消息回复的实体
 * @Author					BOCAI
 * @CreateTime			2017年4月22日		下午2:50:17
 * @Version					V1.0
 * @History
 * Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年4月22日     	BOCAI      	1.0             1.0
 * 
 * Why & What is modified:
 * 
 */
public class Reply {

	/**
	 * 回复编号
	 */
	private String rid;
	
	/**
	 * 回复内容
	 */
	private String rcontent;
	
	/**
	 * 回复时间
	 */
	private String rdatetime;
	
	/**
	 * 消息编号
	 */
	private String mid;
	
	/**
	 * 用户编号
	 */
	private String uid;
	
	/**
	 * 用户实体
	 */
	private User user;
	
	public Reply(){
		super();
	}

	/**
	 * @return 回复编号
	 */
	public String getRid() {
		return rid;
	}

	/**
	 * @param 回复编号
	 */
	public void setRid(String rid) {
		this.rid = rid;
	}

	/**
	 * @return 回复内容
	 */
	public String getRcontent() {
		return rcontent;
	}

	/**
	 * @param 回复内容
	 */
	public void setRcontent(String rcontent) {
		this.rcontent = rcontent;
	}

	/**
	 * @return 回复时间
	 */
	public String getRdatetime() {
		return rdatetime;
	}

	/**
	 * @param 回复时间
	 */
	public void setRdatetime(String rdatetime) {
		this.rdatetime = rdatetime;
	}

	/**
	 * @return 消息编号
	 */
	public String getMid() {
		return mid;
	}

	/**
	 * @param 消息编号
	 */
	public void setMid(String mid) {
		this.mid = mid;
	}

	/**
	 * @return 用户编号
	 */
	public String getUid() {
		return uid;
	}

	/**
	 * @param 用户编号
	 */
	public void setUid(String uid) {
		this.uid = uid;
	}

	/**
	 * @return 用户实体
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param 用户实体
	 */
	public void setUser(User user) {
		this.user = user;
	}
	
	
}
