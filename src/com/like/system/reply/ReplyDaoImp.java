/**
 * @FileName			ReplyDaoImp.java
 * @Author				BOCAI
 * @Version				V1.0
 * @CreateTime		2017年4月22日		下午7:15:25
 * @Copyright			Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.system.reply;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;

import com.like.common.ResultType;
import com.like.common.jdbc.DaoSupport;
import com.like.common.util.StringUtils;
import com.like.system.user.User;


/**
 * @ClassName			ReplyDaoImp
 * @Description			回复消息的数据持久控制器
 * @Author					BOCAI
 * @CreateTime			2017年4月22日		下午7:15:25
 * @Version					V1.0
 * @History
 * Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年4月22日     	BOCAI      	1.0             1.0
 * 
 * Why & What is modified:
 * 
 */
public class ReplyDaoImp extends DaoSupport implements ReplyDao {


	public ReplyDaoImp(){
		this.logger = Logger.getLogger(ReplyDaoImp.class);
	}

	@Override
	public int add(Reply reply) throws Exception {
		int result = ResultType.FAIL;
		
		try{
			this.setConnection(true);
			String sql = ReplyArgument.INSERT_SQL ;
			statement = this.connection.prepareStatement(sql);
			List<String> list = StringUtils.getFieldBySql(sql);
			int location = 1;
			
			for(String field : list){
				statement.setString(location, BeanUtils.getProperty(reply, field));
				location++;	
			}	
			
			result = statement.executeUpdate();
			
		}catch(Exception e){
			this.logger.error(e);
			throw e ;
			
		}finally{
			this.dispose(this.connection, this.statement, this.set);
			
		}
		
		return result;
		
	}

	@Override
	public int delete(Reply reply) throws Exception {
		int result = ResultType.FAIL;
		
		try{
			this.setConnection(true);
			String sql = ReplyArgument.DELETE_SQL;
			statement = this.connection.prepareStatement(sql);
			statement.setString(1, BeanUtils.getProperty(reply, "rid"));
			result = statement.executeUpdate();
			
		}catch(Exception e){
			this.logger.error(e);
			throw e ;
			
		}finally{
			this.dispose(this.connection, this.statement, this.set);
			
		}
		
		return result;
	}

	@Override
	public int update(Reply reply) throws Exception {
		int result = ResultType.FAIL;
		
		try{
			this.setConnection(true);
			String sql = ReplyArgument.UPDATE_SQL;
			String[] Strings =  this.setWhere(reply,1);
			sql = sql + Strings[2];
			statement = this.connection.prepareStatement(sql);
			
			if(Strings[1].length() != 0){
				Strings = Strings[1].split(",");
				int location = 1;
				
				for(String field : Strings){
					statement.setString(location, BeanUtils.getProperty(reply, field));
					location++;
					
				}
			}
			
			result = statement.executeUpdate();
			
		}catch(Exception e){
			this.logger.error(e);
			throw e ;
			
		}finally{
			this.dispose(this.connection, this.statement, this.set);
			
		}
		
		return result;
	}

	@Override
	public List<Reply> list(Reply reply) throws Exception {
		return this.list(reply, 0, -1);
	}

	@Override
	public List<Reply> list(Reply reply, int start, int count) throws Exception {
		return this.list(reply, start, count, "rdatetime");
	}

	@Override
	public List<Reply> list(Reply reply, int start, int count, String orderBy) throws Exception {
		List<Reply> result = new ArrayList<Reply>();
		
		try{
			this.setConnection(true);
			int rowCount = this.size(reply,this.connection);
			String sql = ReplyArgument.QUERY_SQL;
			String[] Strings =  this.setWhere(reply,0);
			sql = sql + Strings[0];
			
			if( orderBy != null && !orderBy.equals("") ){
				sql = sql + " order by " + orderBy + " DESC";
			}
			
			if( count > 0 ){
				
				if( ( start + count ) >= rowCount ){
					count = count - ( start + count - rowCount );
				}
				
				sql = sql + " limit " + start +  "," + count;
			}
			
			this.statement = this.connection.prepareStatement(sql);
			
			if(Strings[1].length() != 0){
				Strings = Strings[1].split(",");
				int location = 1;
				
				for(String field : Strings){
					statement.setString(location, BeanUtils.getProperty(reply, field));
					location++;
				}
				
			}
			
			this.set = this.statement.executeQuery();
			
			while( this.set.next() ){
				Reply obj = new Reply();
				User user = new User();
				BeanUtils.setProperty(obj, "rid", this.set.getString("rid"));
				BeanUtils.setProperty(obj, "rcontent", this.set.getString("rcontent"));
				BeanUtils.setProperty(obj, "rdatetime", this.set.getString("rdatetime").substring(0, 19));
				BeanUtils.setProperty(obj, "mid", this.set.getString("mid"));
				BeanUtils.setProperty(user, "uid", this.set.getString("uid"));
				BeanUtils.setProperty(user, "ualais", this.set.getString("ualais"));
				BeanUtils.setProperty(user, "uimage", this.set.getString("uimage"));
				BeanUtils.setProperty(obj, "user", user);
				result.add(obj);
			}
			
		}catch(Exception e ){
			this.logger.error(e);
			throw e;
			
		}finally{
			this.dispose(connection, statement, set);
			
		}
		
		return result;
	}

	@Override
	public int size(Reply reply) throws Exception {
		int rowCount = 0 ;
		
		try{
			this.setConnection(true);
			rowCount = this.size(reply, connection);
			
		}catch(Exception e ){
			this.logger.error(e);
			throw e;
			
		}finally{
			this.dispose(connection, statement, set);
			
		}
		
		return rowCount ;
	}

	@Override
	public Reply get(String id) throws Exception {
		Reply result = null ;
		
		try{
			this.setConnection(true);
			String sql = ReplyArgument.QUERY_SQL ;
			sql = sql + " and mid=? ";
			this.statement = this.connection.prepareStatement(sql);
			this.statement.setString(1, id);
			this.set = this.statement.executeQuery();
			result = new Reply() ;
			
			if( this.set.next() ){
				User user = new User();
				BeanUtils.setProperty(result, "rid", this.set.getString("rid"));
				BeanUtils.setProperty(result, "rcontent", this.set.getString("rcontent"));
				BeanUtils.setProperty(result, "rdatetime", this.set.getString("rdatetime").substring(0, 19));
				BeanUtils.setProperty(result, "mid", this.set.getString("mid"));
				BeanUtils.setProperty(user, "uid", this.set.getString("uid"));
				BeanUtils.setProperty(user, "ualais", this.set.getString("ualais"));
				BeanUtils.setProperty(user, "uimage", this.set.getString("uimage"));
				BeanUtils.setProperty(result, "user", user);
			}
			
		}catch(Exception e ){
			this.logger.error(e);
			throw e;
			
		}finally{
			this.dispose(connection, statement, set);
			
		}
		
		return result ;
	}

	/**
	 * 
	 * @Description				根据过滤条件获取统计量
	 * @param user				保存过滤条件的信息实体
	 * @param connection		连接对象
	 * @return						返回统计量
	 * @throws Exception
	 */
	private int size(Reply reply,Connection connection) throws Exception {
		int result = 0 ;
		PreparedStatement statement = null ;
		ResultSet set = null ;
		
		try{
			String sql = ReplyArgument.COUNT_SQL ;	
			String[] Strings =  this.setWhere(reply , 0);
			sql = sql + Strings[0];						
			statement = connection.prepareStatement(sql);
			
			if(Strings[1].length() != 0){	
				Strings = Strings[1].split(",");
				int location = 1;
				
				for(String field : Strings){
					statement.setString(location, BeanUtils.getProperty(reply, field));
					location++;
				}
				
			}
		
			set = statement.executeQuery();
			
			if( set.next() ){
				result = Integer.valueOf( set.getString(1) );
			}
			
		}catch(Exception e){
			this.logger.error(e);
			throw e;
			
		}finally{
			this.dispose(null,statement,set);			
			
		}

		return result;
	}
	
	
	/**
	 * 
	 * @Description		拼接过滤条件，统计占位符字段
	 * @param user		占位符的值
	 * @param type		0代表查询，1代表更新
	 * @return				返回过滤条件和占位符字段 
	 * 								数组0表示查询过滤条件
	 * 								数组1表示统计占位符字段
	 * 								数组2表示更新过滤条件
	 * @throws Exception
	 */
	private String[] setWhere(Reply reply, int type) throws Exception{
		StringBuffer where = new StringBuffer();
		StringBuffer field = new StringBuffer();
		String string = new String();
		
		if( reply.getRcontent()!= null && !reply.getRcontent().equals("") ){
			
			if(type == 1){
				string = string + " reply.rcontent=?,";
			}else{
				where.append(" and reply.rcontent=? ");
			}
			
			field.append("rcontent,");
		}
		
		if( reply.getRdatetime()!= null && !reply.getRdatetime().equals("") ){
			
			if(type == 1){
				string = string + " reply.rdatetime=?,";
			}else{
				where.append(" and reply.rdatetime=? ");
			}
			
			field.append("rdatetime,");
		}
		
		if( reply.getMid()!= null && !reply.getMid().equals("") ){
			
			if(type == 1){
				string = string + " reply.mid=?,";
			}else{
				where.append(" and reply.mid=? ");
			}
			
			field.append("mid,");
		}
		
		if( reply.getUid()!= null && !reply.getUid().equals("") ){
			
			if(type == 1){
				string = string + " reply.uid=?,";
			}else{
				where.append(" and reply.uid=? ");
			}
			
			field.append("uid,");
		}
		
		if(type == 1){
			string = string.substring(0, string.length() - 1);
		}
			
		if( reply.getRid()!= null && !reply.getRid().equals("") ){
			
			if(type == 1){
				string = string + " where reply.rid=? ";
			}else{
				where.append(" and reply.rid=? ");
			}

			field.append("rid,");		
		}
		
		String[] Strings = new String[]{where.toString(),field.toString(),string};
		return Strings;
	}

}
