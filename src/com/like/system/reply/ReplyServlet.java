/**
 * @FileName			ReplyServlet.java
 * @Author				BOCAI
 * @Version				V1.0
 * @CreateTime		2017年4月22日		下午2:16:17
 * @Copyright			Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.system.reply;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.like.common.ContextArgument;

/**
 * @ClassName			ReplyServlet
 * @Description			消息回复的视图控制器
 * @Author					BOCAI
 * @CreateTime			2017年4月22日		下午2:16:17
 * @Version					V1.0
 * @History
 * Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年4月22日     	BOCAI      	1.0             1.0
 * 
 * Why & What is modified:
 * 
 */
@SuppressWarnings("serial")
public class ReplyServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		Reply reply = new Reply();
		ReplyService service = new ReplyServiceImp();
		HttpSession session = request.getSession();
		reply.setUid((String)session.getAttribute(ContextArgument.UID));
		String page = request.getParameter("page");
		
		if(page == null){
			page = this.getInitParameter("page");
		}
		
		int p =Integer.valueOf(page) ;
		
		try{
			List<Reply> replylist = service.getReplyList(reply, p);
			int pagesize = service.getReplySize(reply);
			request.setAttribute(ContextArgument.REPLYLIST, replylist);
			request.setAttribute(ContextArgument.PAGESIZE, pagesize);
			request.setAttribute(ContextArgument.PAGE, p);
			request.getRequestDispatcher(ReplyArgument.MYREPLY_JSP).forward(request, response);
			
		}catch(Exception e){
			
			getServletContext().log(e.toString());
			
		}
		
	}

}
