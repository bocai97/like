/**
 * @FileName			ReplyAddServlet.java
 * @Author				BOCAI
 * @Version				V1.0
 * @CreateTime		2017年5月22日		上午10:51:02
 * @Copyright			Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.system.reply;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.like.common.ContextArgument;
import com.like.common.JsonResult;
import com.like.common.ResultType;

import net.sf.json.JSONObject;


/**
 * @ClassName			ReplyAddServlet
 * @Description			添加回复的视图控制器
 * @Author					BOCAI
 * @CreateTime			2017年5月22日		上午10:51:02
 * @Version					V1.0
 * @History
 * Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年5月22日     	BOCAI      	1.0             1.0
 * 
 * Why & What is modified:
 * 
 */
@SuppressWarnings("serial")
public class ReplyAddServlet extends HttpServlet {
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		Reply reply = new Reply();
		ReplyService service = new ReplyServiceImp();
		JsonResult jsonresult = new JsonResult();
		HttpSession session = request.getSession();
		String uid = (String)session.getAttribute(ContextArgument.UID);
		reply.setMid(request.getParameter("mid"));
		reply.setRcontent(request.getParameter("rcontent"));
		reply.setUid(uid);
		
		try{
			if(service.addReply(reply)){
				jsonresult.setStatus(ResultType.SUCCESS);
				jsonresult.setMsg(ReplyArgument.ADD_SUCCESS);

			}else{
				jsonresult.setStatus(ResultType.FAIL);
				jsonresult.setMsg(ReplyArgument.ADD_FAIL);

			}
		response.getWriter().println(JSONObject.fromObject(jsonresult).toString());
			
		}catch(Exception e){
			
			getServletContext().log(e.toString());
			
		}
		
	}

}
