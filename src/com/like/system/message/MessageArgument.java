/**
 * @FileName			MessageArgument.java
 * @Author				BOCAI
 * @Version				V1.0
 * @CreateTime		2017年4月22日		下午2:52:08
 * @Copyright			Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.system.message;

/**
 * @ClassName			MessageArgument
 * @Description			消息的常量类
 * @Author					BOCAI
 * @CreateTime			2017年4月22日		下午2:52:08
 * @Version					V1.0
 * @History
 * Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年4月22日     	BOCAI      	1.0             1.0
 * 
 * Why & What is modified:
 * 
 */
public class MessageArgument {
	
	/**
	 * 插入记录sql语句
	 */
	public static final String INSERT_SQL = "insert into message(mid,mcontent,mdatetime,uid)values(?,?,?,?)";
	
	/**
	 * 删除记录sql语句
	 */
	public static final String DELETE_SQL = "delete from message where mid = ?";
	
	/**
	 * 删除记录sql语句
	 */
	public static final String DELETE_SQL2 = "delete from reply where mid = '{0}'";
	
	/**
	 * 修改记录sql语句
	 */
	public static final String UPDATE_SQL = "update message set";
	
	/**
	 * 查询记录sql语句
	 */
	public static final String QUERY_SQL = "select user.uid,user.ualais,user.uimage,message.mid,message.mcontent,message.mdatetime from user,message where message.uid = user.uid";
	
	/**
	 * 统计记录sql语句
	 */
	public static final String COUNT_SQL = "select count(mid) from message where 1=1 ";
	
	/**
	 * 表示发表成功的提示信息
	 */
	public static final String ADD_SUCCESS = "发表成功" ;
	
	/**
	 * 表示发表失败的提示信息
	 */
	public static final String ADD_FAIL = "发表失败" ;
	
	/**
	 * 表示删除成功的提示信息
	 */
	public static final String DELETE_SUCCESS = "删除成功" ;
	
	/**
	 * 表示删除失败的提示信息
	 */
	public static final String DELETE_FAIL = "删除失败" ;
	
	/**
	 * 表示消息详情JSP页面路径
	 */
	public static final String MESSAGE_INFO_JSP = "./info.jsp" ;
	
	/**
	 * 表示消息列表JSP页面路径
	 */
	public static final String MESSAGE_LIST_JSP = "./list.jsp" ;
	
	/**
	 * 表示个人消息JSP页面路径
	 */
	public static final String MYMESSAGE_JSP = 	"../user/mymessage.jsp" ;
	

}
