/**
 * @FileName			MessageServlet.java
 * @Author				BOCAI
 * @Version				V1.0
 * @CreateTime		2017年4月22日		下午2:53:03
 * @Copyright			Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.system.message;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.like.common.ContextArgument;

/**
 * @ClassName			MessageServlet
 * @Description			消息的视图控制器
 * @Author					BOCAI
 * @CreateTime			2017年4月22日		下午2:53:03
 * @Version					V1.0
 * @History
 * Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年4月22日     	BOCAI      	1.0             1.0
 * 
 * Why & What is modified:
 * 
 */
@SuppressWarnings("serial")
public class MessageServlet extends HttpServlet {
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		Message message = new Message();
		MessageService service = new MessageServiceImp();
		HttpSession session = request.getSession();
		message.setUid((String)session.getAttribute(ContextArgument.UID));
		String page = request.getParameter("page");
		
		if(page == null){
			page = this.getInitParameter("page");
		}
		
		int p =Integer.valueOf(page) ;
		
		try{	
			
			List<Message> messagelist = service.getMessageList(message, p);
			int pagesize = service.getMessageSize(message);
			request.setAttribute(ContextArgument.MESSAGELIST, messagelist);
			request.setAttribute(ContextArgument.PAGESIZE, pagesize);
			request.setAttribute(ContextArgument.PAGE, p);
			request.getRequestDispatcher(MessageArgument.MYMESSAGE_JSP).forward(request, response);
			
		}catch(Exception e){
			
			getServletContext().log(e.toString());
			
		}
		
	}

}
