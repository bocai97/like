/**
 * @FileName			MessageInfoServlet.java
 * @Author				BOCAI
 * @Version				V1.0
 * @CreateTime		2017年5月23日		下午10:03:01
 * @Copyright			Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.system.message;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.like.common.ContextArgument;
import com.like.system.reply.Reply;
import com.like.system.reply.ReplyService;
import com.like.system.reply.ReplyServiceImp;

/**
 * @ClassName			MessageInfoServlet
 * @Description			消息详情的视图控制器
 * @Author					BOCAI
 * @CreateTime			2017年5月23日		下午10:03:01
 * @Version					V1.0
 * @History
 * Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年5月23日     	BOCAI      	1.0             1.0
 * 
 * Why & What is modified:
 * 
 */
@SuppressWarnings("serial")
public class MessageInfoServlet extends HttpServlet {
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		Message message = new Message();
		Reply reply = new Reply();
		MessageService messageservice = new MessageServiceImp();
		ReplyService replyservice = new ReplyServiceImp();
		String page = request.getParameter("page");
		message.setMid(request.getParameter("mid"));
		reply.setMid(request.getParameter("mid"));
		
		if(page == null){
			page = this.getInitParameter("page");
		}
		
		int p =Integer.valueOf(page) ;

		try{
			Message messageinfo = messageservice.getMessagelnfo(message);
			int pagesize = replyservice.getReplySize(reply);
			List<Reply> replylist = replyservice.getReplyList(reply, p);
			String mid = message.getMid();
			request.setAttribute(ContextArgument.MESSAGEINFO, messageinfo);
			request.setAttribute(ContextArgument.PAGESIZE, pagesize);
			request.setAttribute(ContextArgument.REPLYLIST, replylist);
			request.setAttribute(ContextArgument.MID, mid);
			request.getRequestDispatcher(MessageArgument.MESSAGE_INFO_JSP).forward(request, response);
			
		}catch(Exception e){
			
			getServletContext().log(e.toString());
			
		}
		
	}

}
