/**
 * @FileName			MessageListServlet.java
 * @Author				BOCAI
 * @Version				V1.0
 * @CreateTime		2017年5月23日		下午9:59:10
 * @Copyright			Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.system.message;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.like.common.ContextArgument;


/**
 * @ClassName			MessageListServlet
 * @Description			消息列表的视图控制器
 * @Author					BOCAI
 * @CreateTime			2017年5月23日		下午9:59:10
 * @Version					V1.0
 * @History
 * Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年5月23日     	BOCAI      	1.0             1.0
 * 
 * Why & What is modified:
 * 
 */
@SuppressWarnings("serial")
public class MessageListServlet extends HttpServlet {
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		Message message = new Message();
		MessageService service = new MessageServiceImp();
		String page = request.getParameter("page");
		
		if(page == null){
			page = this.getInitParameter("page");
		}
		
		int p =Integer.valueOf(page) ;
		
		try{	
			List<Message> messagelist = service.getMessageList(message, p);
			int pagesize = service.getMessageSize(message);
			request.setAttribute(ContextArgument.MESSAGELIST, messagelist);
			request.setAttribute(ContextArgument.PAGESIZE, pagesize);
			request.setAttribute(ContextArgument.PAGE, p);
			request.getRequestDispatcher(MessageArgument.MESSAGE_LIST_JSP).forward(request, response);
			
		}catch(Exception e){
			
			getServletContext().log(e.toString());
			
		}
		
	}

}
