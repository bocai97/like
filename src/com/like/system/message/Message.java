/**
 * @FileName			Message.java
 * @Author				BOCAI
 * @Version				V1.0
 * @CreateTime		2017年4月22日		下午2:51:48
 * @Copyright			Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.system.message;

import com.like.system.user.User;

/**
 * @ClassName			Message
 * @Description			消息的实体
 * @Author					BOCAI
 * @CreateTime			2017年4月22日		下午2:51:48
 * @Version					V1.0
 * @History
 * Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年4月22日     	BOCAI      	1.0             1.0
 * 
 * Why & What is modified:
 * 
 */
public class Message {

	/**
	 * 消息编号
	 */
	private String mid;
	
	/**
	 * 消息内容
	 */
	private String mcontent;
	
	/**
	 * 消息发布时间
	 */
	private String mdatetime;
	
	/**
	 * 用户编号
	 */
	private String uid;
	
	/**
	 * 用户实体
	 */
	private User user;
	
	public Message(){
		super();
	}

	/**
	 * @return 消息编号
	 */
	public String getMid() {
		return mid;
	}

	/**
	 * @param 消息编号
	 */
	public void setMid(String mid) {
		this.mid = mid;
	}

	/**
	 * @return 消息内容
	 */
	public String getMcontent() {
		return mcontent;
	}

	/**
	 * @param 消息内容
	 */
	public void setMcontent(String mcontent) {
		this.mcontent = mcontent;
	}

	/**
	 * @return 消息发布时间
	 */
	public String getMdatetime() {
		return mdatetime;
	}

	/**
	 * @param 消息发布时间
	 */
	public void setMdatetime(String mdatetime) {
		this.mdatetime = mdatetime;
	}

	/**
	 * @return 用户编号
	 */
	public String getUid() {
		return uid;
	}

	/**
	 * @param 用户编号
	 */
	public void setUid(String uid) {
		this.uid = uid;
	}

	/**
	 * @return 用户实体
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param 用户实体
	 */
	public void setUser(User user) {
		this.user = user;
	}
	
	
}
