/**
 * @FileName			MessageDao.java
 * @Author				BOCAI
 * @Version				V1.0
 * @CreateTime		2017年4月22日		下午2:52:19
 * @Copyright			Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.system.message;

import java.util.List;



/**
 * @ClassName			MessageDao
 * @Description			消息的数据持久控制器
 * @Author					BOCAI
 * @CreateTime			2017年4月22日		下午2:52:19
 * @Version					V1.0
 * @History
 * Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年4月22日     	BOCAI      	1.0             1.0
 * 
 * Why & What is modified:
 * 
 */
public interface MessageDao {
	
	/**
	 * 
	 * 新增当前消息
	 * @param message	消息实体
	 * @return				成功返回 >0 的数字，否则返回 <=0 的数字
	 * @throws Exception
	 */
	public int add( Message message ) throws Exception;
	
	/**
	 * 
	 * 删除当前消息
	 * @param message	消息实体
	 * @return				成功返回 >0 的数字，否则返回 <=0 的数字
	 * @throws Exception
	 */
	public int delete( Message message ) throws Exception;
	
	/**
	 * 
	 * 修改当前消息
	 * @param message	消息实体
	 * @return				成功返回 >0 的数字，否则返回 <=0 的数字
	 * @throws Exception
	 */
	public int update( Message message ) throws Exception;
		
	/**
	 * 
	 * 根据查询条件获取所有消息
	 * @param message	保存过滤条件的消息
	 * @return				返回List<Message>类型对象，否则返回null
	 * @throws Exception
	 */
	public List<Message> list( Message message ) throws Exception;	
	
	/**
	 * 
	 * 根据查询条件获取所有消息
	 * @param message	保存过滤条件的消息
	 * @param start		分页条件的起始行号
	 * @param count		分页条件的单页记录数
	 * @return				返回List<Message>类型对象，否则返回null
	 * @throws Exception
	 */
	public List<Message> list( Message message , int start , int count )throws Exception;

	/**
	 * 
	 * 根据查询条件获取所有消息
	 * @param message	保存过滤条件的消息
	 * @param start		分页条件的起始行号
	 * @param count		分页条件的单页记录数
	 * @param orderBy	排序条件
	 * @return				返回List<Message>类型对象，否则返回null
	 * @throws Exception
	 */
	public List<Message> list( Message message , int start , int count , String orderBy ) throws Exception;
	
	/**
	 * 
	 * 根据过滤条件获取消息记录行的总量
	 * @param message	过滤条件
	 * @return				返回总量，否则返回0
	 * @throws Exception
	 */
	public int size( Message message ) throws Exception;
	
	/**
	 * 
	 * 根据ID获取完整的消息
	 * @param id			消息编号
	 * @return				成功返回Message类型对象，否则返回null
	 * @throws Exception
	 */
	public Message get(String id) throws Exception ;

}
