/**
 * @FileName			MessageServiceImp.java
 * @Author				BOCAI
 * @Version				V1.0
 * @CreateTime		2017年4月22日		下午7:16:22
 * @Copyright			Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.system.message;

import java.util.List;

import com.like.common.ResultType;
import com.like.common.util.IdUtils;


/**
 * @ClassName			MessageServiceImp
 * @Description			消息的业务控制器
 * @Author					BOCAI
 * @CreateTime			2017年4月22日		下午7:16:22
 * @Version					V1.0
 * @History
 * Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年4月22日     	BOCAI      	1.0             1.0
 * 
 * Why & What is modified:
 * 
 */
public class MessageServiceImp implements MessageService {
	
	private MessageDao dao;
	
	public MessageServiceImp(){
		this.dao =  new MessageDaoImp();
	}
	

	@Override
	public boolean addMessage(Message message) throws Exception {
		boolean result = false;
		
		if( !messageisNull(message, 1)){
			return result;
		}
		
		message.setMid(IdUtils.getUUID());
		int type = this.dao.add(message);
		result = ResultType.get(type);
		return result;
	}

	@Override
	public boolean deleteMessage(Message message) throws Exception {
		boolean result = false;
		
		if( !messageisNull(message, 2)){
			return result;
		}
		
		int size = this.dao.size(message);
		
		if(size != 0){
			int type = this.dao.delete(message);
			result = ResultType.get(type);
		}
		
		return result;
	}


	@Override
	public List<Message> getMessageList(Message message, int page) throws Exception {
		List<Message> result = null;
		int start = (page - 1) * 10;
		result = this.dao.list(message, start, 10);
		return result;
	}


	@Override
	public Message getMessagelnfo(Message message) throws Exception {
		Message result = new Message();
		
		if( !messageisNull(message, 2)){
			return result;
		}
		
		String id = message.getMid();
		result = this.dao.get(id);
		return result;
	}
	
	@Override
	public int getMessageSize(Message message) throws Exception {
		int result = 0;
		int size = this.dao.size(message);
//		分页总量 = （总记录数 + 分页记录数-1）/分页记录数
		result = ( size + 10 - 1) / 10;
		return result;
	}
	
	
	/**
	 * 
	 * 判断输入信息是否为空
	 * @param message		消息实体
	 * @param type			1表示过滤消息内容和用户编号，2表示过滤消息编号
	 * @return
	 */
	private boolean messageisNull( Message message, int type ){
		boolean result = false;
		
		if( type == 1){
			
			if( message.getMcontent() == null || message.getMcontent().equals("")){
				return result;
			}
			
			if( message.getUid() == null || message.getUid().equals("")){
				return result;
			}
			
		}
		
		if(type == 2){
			
			if( message.getMid() == null || message.getMid().equals("")){
				return result;
			}
			
		}

		result = true;
		return result;
	}


}
