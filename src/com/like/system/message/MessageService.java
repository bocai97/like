/**
 * @FileName			MessageService.java
 * @Author				BOCAI
 * @Version				V1.0
 * @CreateTime		2017年4月22日		下午2:52:42
 * @Copyright			Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.system.message;

import java.util.List;

/**
 * @ClassName			MessageService
 * @Description			消息的业务控制器
 * @Author					BOCAI
 * @CreateTime			2017年4月22日		下午2:52:42
 * @Version					V1.0
 * @History
 * Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年4月22日     	BOCAI      	1.0             1.0
 * 
 * Why & What is modified:
 * 
 */
public interface MessageService {
	
	/**
	 * 
	 * 添加消息
	 * @param message			消息实体
	 * @return						成功返回true，否则返回false
	 * @throws Exception
	 */
	public boolean addMessage(Message message) throws Exception;
	
	/**
	 * 
	 * 删除消息
	 * @param message			消息实体
	 * @return						成功返回true，否则返回false
	 * @throws Exception
	 */
	public boolean deleteMessage(Message message) throws Exception;
	
	/**
	 * 
	 * 获取分页消息列表
	 * @param message		消息实体
	 * @param page			页码
	 * @return					返回List<Message>类型对象，否则返回null
	 * @throws Exception
	 */
	public List<Message> getMessageList(Message message, int page) throws Exception;
	
	/**
	 * 
	 * 获取消息详情
	 * @param message		消息实体
	 * @return					返回Message类型对象，否则返回null
	 * @throws Exception
	 */
	public Message getMessagelnfo(Message message) throws Exception;
	
	
	/**
	 * 
	 * 获取消息总页数
	 * @param message		消息实体
	 * @return
	 * @throws Exception	返回消息总页数，否则返回0
	 */
	public int getMessageSize(Message message) throws Exception;


}
