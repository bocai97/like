/**
 * @FileName			StringUtil.java
 * @Author				BOCAI
 * @Version				V1.0
 * @CreateTime		2017年5月17日		下午1:23:43
 * @Copyright			Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.common.util;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName			StringUtil
 * @Description			字符串操作工具
 * @Author					BOCAI
 * @CreateTime			2017年5月17日		下午1:23:43
 * @Version					V1.0
 * @History
 * Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年5月17日     	BOCAI      	1.0             1.0
 * 
 * Why & What is modified:
 * 
 */
public class StringUtils {
	

	/**
	 * 
	 * 通过sql语句获得字段名
	 * @param sql			sql语句字符串
	 * @return				返回List<String>类型对象，否则返回null
	 */
	public static List<String> getFieldBySql(String sql) {
		List<String> list = new ArrayList<String>();
		String [] strings = sql.split("\\(");//通过"("分割sql语句
		strings = strings[1].split("\\)");//通过")"分割字符串
		strings = strings[0].split(",");//通过","分割字符串，得到字段数组
		
		for(String string : strings){//遍历字符串数组，将字符串添加到list中
			list.add(string);
		}
		
		return list;		
	}

}
