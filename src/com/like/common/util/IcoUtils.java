/**
 * @FileName			IcoUtils.java
 * @Author				BOCAI
 * @Version				V1.0
 * @CreateTime		2017年5月24日		下午10:36:58
 * @Copyright			Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.common.util;

import java.util.Random;

/**
 * @ClassName			IcoUtils
 * @Description			头像工具类
 * @Author					BOCAI
 * @CreateTime			2017年5月24日		下午10:36:58
 * @Version					V1.0
 * @History
 * Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年5月24日     	BOCAI      	1.0             1.0
 * 
 * Why & What is modified:
 * 
 */
public class IcoUtils {
	
	/**
	 * 
	 * 随机生成一个头像名称
	 * @return				返回随机生成的头像名称
	 */
	public static String  icoCreate(){
		String result = null;
		int number = new Random().nextInt(15) + 1;
		result = "ico" + number + ".png";
		return result;
	}

}
