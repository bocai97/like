/**
 * @FileName			IdUtils.java
 * @Author				BOCAI
 * @Version				V1.0
 * @CreateTime		2017年5月20日		下午5:49:07
 * @Copyright			Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.common.util;

import java.util.UUID;

/**
 * @ClassName			IdUtils
 * @Description			用于自动生成id的工具类
 * @Author					BOCAI
 * @CreateTime			2017年5月20日		下午5:49:07
 * @Version					V1.0
 * @History
 * Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年5月20日     	BOCAI      	1.0             1.0
 * 
 * Why & What is modified:
 * 
 */
public class IdUtils {
	
	
	/**
	 * 
	 * 获取自动生成的UUID
	 * @return				返回自动生成的UUID
	 */
	public static String getUUID() {

		return UUID.randomUUID().toString();

	}

}
