/**
 * @FileName			ResultType.java
 * @Author				BOCAI
 * @Version				V1.0
 * @CreateTime		2017年4月22日		下午7:22:34
 * @Copyright			Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.common;

/**
 * @ClassName			ResultType
 * @Description			操作结果的常量类
 * @Author					BOCAI
 * @CreateTime			2017年4月22日		下午7:22:34
 * @Version					V1.0
 * @History
 * Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年4月22日     	BOCAI      	1.0             1.0
 * 
 * Why & What is modified:
 * 
 */
public class ResultType {
	
	/**
	 * 操作成功
	 */
	public final static int SUCCESS = 1 ;
	
	/**
	 * 操作结果默认值
	 */
	public final static int DEFAULT = 0 ;
	
	/**
	 * 操作失败
	 */
	public final static int FAIL = -1 ;
	
	/**
	 * 
	 * 将操作结果转换成boolean类型值
	 * @param type		操作结果常量
	 * @return				大于0返回true,否则返回false
	 */
	public static boolean get( int type ){
		boolean result = false ;
		result = type > 0 ? true : false ;
		return result ;
	}

}
