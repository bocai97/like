/**
 * @FileName			JsonResult.java
 * @Author				BOCAI
 * @Version				V1.0
 * @CreateTime		2017年5月23日		下午3:09:50
 * @Copyright			Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.common;

/**
 * @ClassName			JsonResult
 * @Description			Json类型返回值实体
 * @Author					BOCAI
 * @CreateTime			2017年5月23日		下午3:09:50
 * @Version					V1.0
 * @History
 * Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年5月23日     	BOCAI      	1.0             1.0
 * 
 * Why & What is modified:
 * 
 */
public class JsonResult {
	
	/**
	 * 状态代码
	 */
	private int status;
	
	/**
	 * 返回信息
	 */
	private String msg;
	
	/**
	 * 返回数据
	 */
	private String jsonData;

	/**
	 * @return 状态代码
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @param 状态代码
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * @return 返回信息
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * @param 返回信息
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * @return 返回数据
	 */
	public String getJsonData() {
		return jsonData;
	}

	/**
	 * @param 返回数据
	 */
	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}
	
	

}
