/**
 * @FileName			ContextArgument.java
 * @Author				BOCAI
 * @Version				V1.0
 * @CreateTime		2017年4月22日		下午1:58:19
 * @Copyright			Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.common;

/**
 * @ClassName			ContextArgument
 * @Description			全局参数的常量类
 * @Author					BOCAI
 * @CreateTime			2017年4月22日		下午1:58:19
 * @Version					V1.0
 * @History
 * Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年4月22日     	BOCAI      	1.0             1.0
 * 
 * Why & What is modified:
 * 
 */
public class ContextArgument {
	
	/**
	 * 表示保存用户编号的session关键名
	 */
	public static final String UID = "uid" ;
	
	/**
	 * 表示保存用户昵称的session关键名
	 */
	public static final String UALAIS = "ualais" ;
	
	/**
	 * 表示保存用户信息的关键名
	 */
	public static final String USER = "user" ;
	
	/**
	 * 表示保存最大页码的关键名
	 */
	public static final String PAGESIZE = "pagesize" ;
	
	/**
	 * 表示保存页码的关键名
	 */
	public static final String PAGE = "page" ;
	
	/**
	 * 表示保存回复列表的关键名
	 */
	public static final String REPLYLIST = "replylist" ;
	
	/**
	 * 表示保存消息编号的关键名
	 */
	public static final String MID = "mid" ;
	
	/**
	 * 表示保存消息列表的关键名
	 */
	public static final String MESSAGELIST = "messagelist" ;
	
	/**
	 * 表示保存消息详情的关键名
	 */
	public static final String MESSAGEINFO = "messageinfo" ;
	
	/**
	 * 表示操作成功的提示信息
	 */
	public static final String SUCCESS = "操作成功" ;
	
	/**
	 * 表示操作失败的提示信息
	 */
	public static final String FAIL = "操作失败" ;
	

}
