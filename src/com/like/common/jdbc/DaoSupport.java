/**
 * @FileName			DaoSupport.java
 * @Author				BOCAI
 * @Version				V1.0
 * @CreateTime		2017年5月13日		上午2:03:58
 * @Copyright			Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.common.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;


/**
 * @ClassName			DaoSupport
 * @Description			数据持久控制器的基类
 * @Author					BOCAI
 * @CreateTime			2017年5月13日		上午2:03:58
 * @Version					V1.0
 * @History
 * Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年5月13日     	BOCAI      	1.0             1.0
 * 
 * Why & What is modified:
 * 
 */
public abstract class DaoSupport {
	
	/**
	 * 数据库连接对象
	 */
	protected Connection connection;
	/**
	 * SQL命令执行者
	 */
	protected PreparedStatement statement; 
	/**
	 * 查询结果集
	 */
	protected ResultSet set;
	/**
	 * 日志信息控制器
	 */
	protected Logger logger ;
	
	
	/**
	 * 
	 * 设置连接对象
	 * @param isAuto		是否开启事务
	 * @throws Exception
	 */
	protected void setConnection(boolean isAuto) throws  Exception{
		
		if( this.connection == null || this.connection.isClosed() ){
			this.connection = ConnectionBuilder.get();
			this.connection.setAutoCommit(isAuto);
		}
		
	}
	
	
	/**
	 * 
	 * 关闭相关组件，并释放对象资源
	 * @param connection		连接对象
	 * @param statement		SQL命令执行者
	 * @param set					查询结果集
	 * @throws SQLException
	 */
	protected void dispose(Connection connection,PreparedStatement statement,ResultSet set) throws SQLException{
		
		if(set != null ){
			set.close();
			set = null ;
		}
		
		if( statement != null ){
			statement.close();
			statement = null ;
		}
		
		if( connection != null && ! connection.isClosed()){
			connection.close();
			connection = null ;
		}
			
	}
	
}
