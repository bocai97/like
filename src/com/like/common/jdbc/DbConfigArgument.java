/**
 * @FileName			DbConfigArgument.java
 * @Author				BOCAI
 * @Version				V1.0
 * @CreateTime		2017年5月13日		上午12:14:24
 * @Copyright			Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.common.jdbc;

/**
 * @ClassName			DbConfigArgument
 * @Description			数据库配置参数的常量类
 * @Author					BOCAI
 * @CreateTime			2017年5月13日		上午12:14:24
 * @Version					V1.0
 * @History
 * Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年5月13日     	BOCAI      	1.0             1.0
 * 
 * Why & What is modified:
 * 
 */
public class DbConfigArgument {
	
	/**
	 * 表示数据库连接池配置文件路径的关键名
	 */
	public static final String CONFIGPATH = "\\dbcpConfig.properties";
	
}
