/**
 * @FileName			DbConfig.java
 * @Author				BOCAI
 * @Version				V1.0
 * @CreateTime		2017年4月22日		下午7:35:51
 * @Copyright			Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.common.jdbc;

/**
 * @ClassName			DbConfig
 * @Description			数据库配置参数的信息实体
 * @Author					BOCAI
 * @CreateTime			2017年4月22日		下午7:35:51
 * @Version					V1.0
 * @History
 * Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年4月22日     	BOCAI      	1.0             1.0
 * 
 * Why & What is modified:
 * 
 */
public class DbConfig {
	/**
	 * 驱动包主类名
	 */
	private String driverClassName;

	/**
	 * 连接数据库的URL
	 */
	private String url;

	/**
	 * 用户名
	 */
	private String username;

	/**
	 * 密码
	 */
	private String password;
	
	/**
	 * 最小空闲连接
	 */
	private int minIdle;
	
	/**
	 * 最大空闲连接
	 */
	private int maxIdle;
	
	/**
	 * 最大连接数量
	 */
	private int maxActive;
	
	/**
	 * 连接类型：C3P0\DBCP\JNDI
	 */
	private String type ;
	
	
	
	public DbConfig() {

	}
	
	public DbConfig(String driverClassName, String url, String username, String password, int minIdle, int maxIdle, int maxActive) {
		super();
		this.driverClassName = driverClassName;
		this.url = url;
		this.username = username;
		this.password = password;
		this.minIdle = minIdle;
		this.maxIdle = maxIdle;
		this.maxActive = maxActive;
	}

	/**
	 * 获取驱动包主类名
	 * 
	 * @return 驱动包主类名
	 */
	public String getDriverClassName() {
		return driverClassName;
	}

	/**
	 * 设置驱动包主类名
	 * @param driverClass 驱动包主类名
	 */
	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName;
	}

	/**
	 * 获取连接数据库的URL
	 * 
	 * @return 连接数据库的URL
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * 设置连接数据库的URL
	 * 
	 * @param url	连接数据库的URL
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * 获取用户名
	 * 
	 * @return 用户名
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * 设置用户名
	 * 
	 * @param name	用户名
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * 获取密码
	 * 
	 * @return 密码
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * 设置密码
	 * 
	 * @param password	密码
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	/**
	 * 获取最小空闲连接
	 * 
	 * @return 最小空闲连接
	 */
	public int getMinIdle() {
		return minIdle;
	}

	/**
	 * 设置最小空闲连接
	 * 
	 * @param minIdle  最小空闲连接
	 */
	public void setMinIdle(int minIdle) {
		this.minIdle = minIdle;
	}

	/**
	 * 获取最大空闲连接
	 * 
	 * @return 最大空闲连接
	 */
	public int getMaxIdle() {
		return maxIdle;
	}

	/**
	 * 设置最大空闲连接
	 * 
	 * @param maxIdle 最大空闲连接
	 */
	public void setMaxIdle(int maxIdle) {
		this.maxIdle = maxIdle;
	}

	/**
	 * 获取最大连接数量
	 * 
	 * @return 最大连接数量
	 */
	public int getMaxActive() {
		return maxActive;
	}

	/**
	 * 设置最大连接数量
	 * 
	 * @param maxActive 最大连接数量
	 */
	public void setMaxActive(int maxActive) {
		this.maxActive = maxActive;
	}

	/**
	 * 获取连接类型
	 * 
	 * @return 连接类型：C3P0\DBCP\JNDI
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * 设置连接类型
	 * 
	 * @param type 连接类型：C3P0\DBCP\JNDI
	 */
	public void setType(String type) {
		this.type = type;
	}
	

}
