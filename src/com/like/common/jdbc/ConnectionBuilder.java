/**
 * @FileName ConnectionBuilder.java
 * @Author BOCAI
 * @Version V1.0
 * @CreateTime 2017年4月22日        下午7:36:21
 * @Copyright Copyright(C) 2016-2017 All rights Reserved, Designed By BOCAI
 */
package com.like.common.jdbc;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.log4j.Logger;

import java.io.InputStream;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.util.Properties;


/**
 * @ClassName ConnectionBuilder
 * @Description 数据库连接对象的创建者
 * @Author BOCAI
 * @CreateTime 2017年4月22日        下午7:36:21
 * @Version V1.0
 * @History Date         Author        Version        Discription
 * ---------------------------------------------------------------------------
 * 2017年4月22日     	BOCAI      	1.0             1.0
 * <p>
 * Why & What is modified:
 */
public class ConnectionBuilder {

    /**
     * 连接池
     */
    private static BasicDataSource dataSource;

    /**
     * 日志控制器
     */
    private static Logger logger = Logger.getLogger(ConnectionBuilder.class);

    /**
     * 获取数据库配置参数的实体
     *
     * @return 返回DbConfig类型的对象
     * @throws Exception
     */
    private static DbConfig getDbConfig() throws Exception {
        DbConfig config = new DbConfig();
        InputStream inputStream = null;
        try {
            Properties properties = new Properties();
            inputStream = ConnectionBuilder.class.getClassLoader().getResourceAsStream(DbConfigArgument.CONFIGPATH);
            properties.load(inputStream);
            Field[] fields = config.getClass().getDeclaredFields();

            for (Field field : fields) {
                field.setAccessible(true);
                BeanUtils.setProperty(config, field.getName(), properties.getProperty(field.getName()));
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;

        } finally {
            inputStream.close();

        }

        return config;
    }

    /**
     * 获取数据库连接对象
     *
     * @return 返回Connection类型的对象
     * @throws Exception
     */
    public static Connection get() throws Exception {
        DbConfig config = getDbConfig();
        Connection result = null;

        if (config.getType().equalsIgnoreCase("dbcp")) {
            result = dbcp();

        }

        return result;
    }

    /**
     * 获取数据库连接对象（使用DBCP连接池方式）
     *
     * @return 返回Connection类型的对象
     * @throws Exception
     */
    public static Connection dbcp() throws Exception {
        Connection result = null;

        try {

            if (dataSource == null) {
                DbConfig config = getDbConfig();
                dataSource = new BasicDataSource();
                BeanUtils.copyProperties(dataSource, config);

            }

            result = dataSource.getConnection();

        } catch (Exception e) {
            logger.error("方法getDbcpConnection()出错：" + e.getMessage());
            throw e;

        }

        return result;

    }
}
