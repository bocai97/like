/**
 * 删除消息
 */
function deletemessage(mid) {

	if(confirm("确定要删除这条消息？")) {

		//设置提交数据
		var postData = {
			mid: mid
		}

		$.post("../message/delete.do",
			postData,
			//当后台返回时接收
			function(data) {
				if(data.status == 1) {
					alert(data.msg);
					window.location.reload();
				} else {
					alert(data.msg);
				}

			}, 'json');
	}

}