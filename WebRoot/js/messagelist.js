/**
 * 添加消息
 */
function addmessage() {
	//获取输入框中的数据
	var mcontent = $('#mcontent').val();

	//非空判断
	if(mcontent == null || mcontent == '') {
		$('#tip').html('内容不能为空！');
		return;
	}

	//设置提交数据
	var postData = {
		mcontent: mcontent
	}

	$.post("./add.do",
		postData,
		//当后台返回时接收
		function(data) {
			if(data.status == 1) {
				$('#tip').html(data.msg);
				window.location.reload();
			} else {
				$('#tip').html(data.msg);
			}

		}, 'json');
}

/**
 * 清空输入框
 */
function clean() {

	$('#mcontent').val('');
	$('#tip').html("");

}