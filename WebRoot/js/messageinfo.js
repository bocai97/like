/**
 * 添加回复
 */
function addreply(mid) {
	//获取输入框中的数据
	var rcontent = $('#rcontent').val();

	//非空判断
	if(rcontent == null || rcontent == '') {
		$('#tip').html('回复内容不能为空！');
		return;
	}

	//设置提交数据
	var postData = {
		mid: mid,
		rcontent: rcontent
	}

	$.post("../reply/add.do",
		postData,
		//当后台返回时接收
		function(data) {
			if(data.status == 1) {
				$('#tip').html(data.msg);
				window.location.reload();
			} else {
				$('#tip').html(data.msg);
			}

		}, 'json');
}

/**
 * 清空输入框
 */
function clean() {

	$('#rcontent').val('');
	$('#tip').html("");

}