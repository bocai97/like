/**
 * 切换登录/注册选项卡
 */
$(function() {
	$("#regbox").hide();
	$(".tab").click(function() {
		var KEY = $(this).attr('id');

		if(KEY == 'reg') {
			$("#login").removeClass('active');
			$("#reg").addClass('active');
			$("#loginbox").slideUp();
			$("#regbox").slideDown();
		} else {
			$("#reg").removeClass('active');
			$("#login").addClass('active');
			$("#regbox").slideUp();
			$("#loginbox").slideDown();
		}
	});
});

/**
 * 清空输入框
 */
function clean() {
	$('#rb_ulogon').val("");
	$('#rb_ualais').val("");
	$('#rb_upasswd').val("");
	$('#rb_upasswd2').val("");
}

/**
 * 登录
 */
function login() {
	//获取输入框中的数据
	var ulogon = $('#lb_ulogon').val();
	var upasswd = $('#lb_upasswd').val();

	//非空判断
	if(ulogon == null || ulogon == '') {
		$('#lb_tip').html('用户名不允许为空');
		return;
	}
	
	if(upasswd == null || upasswd == '') {
		$('#lb_tip').html('请输入密码');
		return;
	}

	var psd = md5(upasswd);

	//设置提交数据
	var postData = {
		ulogon: ulogon,
		upasswd: psd
	}

	$.post("./login.do",
		postData,
		//当后台返回时接收
		function(data) {
			if(data.status == 1) {
				$('#lb_tip').html(data.msg);
				window.location.href = data.jsonData;
			} else {
				$('#lb_tip').html(data.msg);
			}

		}, 'json');
}

/**
 * 注册
 */
function reg() {
	//获取输入框中的数据
	var ulogon = $('#rb_ulogon').val();
	var ualais = $('#rb_ualais').val();
	var upasswd = $('#rb_upasswd').val();
	var upasswd2 = $('#rb_upasswd2').val();

	//非空判断
	if(ulogon == null || ulogon == '') {
		$('#rb_tip').html('用户名不允许为空');
		return;
	}

	if(ualais == null || ualais == '') {
		$('#rb_tip').html('昵称不允许为空');
		return;
	}

	if(upasswd == null || upasswd == '') {
		$('#rb_tip').html('密码不能为空');
		return;
	}

	if(upasswd != upasswd2) {
		$('#rb_tip').html('输入两次密码不相同');
		return;
	}

	var psd = md5(upasswd);
	
	//设置提交数据
	var postData = {
		ulogon: ulogon,
		ualais: ualais,
		upasswd: psd
	}

	$.post("./register.do",
		postData,
		//当后台返回时接收
		function(data) {
			if(data.status == 1) {
				$('#rb_tip').html(data.msg);
				clean();
			} else {
				$('#rb_tip').html(data.msg);

			}
		}, 'json');
}