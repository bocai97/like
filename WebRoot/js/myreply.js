/**
 * 删除回复
 */
function deletereply(rid) {

	if(confirm("确定要删除这条回复？")) {

		//设置提交数据
		var postData = {
			rid: rid
		}

		$.post("../reply/delete.do",
			postData,
			//当后台返回时接收
			function(data) {
				if(data.status == 1) {
					alert(data.msg);
					window.location.reload();
				} else {
					alert(data.msg);
				}

			}, 'json');
	}

}