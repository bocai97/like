/**
 * 修改个人资料
 */
function updateuser() {
	//获取输入框中的数据
	var ualais = $('#ualais').val();
	var usex = $('#usex').val();
	var uinfo = $('#uinfo').val();

	//非空判断
	if(ualais == null || ualais == '') {
		alert("昵称不能为空");
		return;
	}

	if(usex == null || usex == '') {
		alert("性别不能为空");
		return;
	}

	//设置提交数据
	var postData = {
		ualais: ualais,
		usex: usex,
		uinfo: uinfo
	}

	$.post("./update.do",
		postData,
		//当后台返回时接收
		function(data) {
			if(data.status == 1) {
				alert("修改成功！");
				window.location.reload();
			} else {
				alert("修改失败！");
			}

		}, 'json');
}