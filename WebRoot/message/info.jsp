<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<title>like</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css" />
		<script src="${pageContext.request.contextPath}/js/jquery.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="${pageContext.request.contextPath}/js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="${pageContext.request.contextPath}/js/messageinfo.js" type="text/javascript" charset="utf-8"></script>
	</head>
	<body>
		<!--布局容器-->
		<div class="container">
			<header class="page-header">
			</header>
			<!--导航条-->
			<nav class='navbar navbar-inverse navbar-fixed-top'>
				<div class="container">
					<div class="navbar-header">
						<a href="" class='navbar-brand'>
							Like
						</a>
						<button class='navbar-toggle collapsed' data-toggle='collapse' data-target='#mynavbar'>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					<div id="mynavbar" class='collapse navbar-collapse'>
						<ul class='nav navbar-nav'>
							<li class='active'>
								<a href="${pageContext.request.contextPath}/message/list.do">消息广场</a>
							</li>
							<li>
								<a href="${pageContext.request.contextPath}/user/info.do">个人中心</a>
							</li>
						</ul>

						<ul class='nav navbar-nav navbar-right'>
							<li>
								<a href="${pageContext.request.contextPath}/user/info.do">${sessionScope.ualais}</a>
							</li>
							<li>
								<a href="${pageContext.request.contextPath}/user/exit.do">退出</a>
							</li>
						</ul>
					</div>
				</div>
			</nav>
			<!--导航条结束-->
			<!--页面主体-->
			<div class="row">
				<div class="col-md-1">
				</div>
				<!--界面-->
				<div class="col-md-9">
					<!--内容卡片-->
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="media">
										<div class="media-left">
											<img class="media-object img-rounded" src="${pageContext.request.contextPath}/img/${requestScope.messageinfo.user.uimage}" width="80px" height="80px">
										</div>
										<div class="media-body">
											<h4 class="media-heading">${requestScope.messageinfo.user.ualais}</h4>
											<div>${requestScope.messageinfo.mdatetime}</div>
											<p>${requestScope.messageinfo.mcontent}</p>
										</div>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-body">
									<input type="text" class="form-control" placeholder="发表回复" id="rcontent">
								</div>
								<div class="panel-footer">
									<a class="btn btn-default" role="button" onclick="addreply('${requestScope.mid}')"><span class="glyphicon glyphicon-ok"></span>&nbsp;发表</a>
									<a class="btn btn-default" role="button" onclick="clean()"><span class="glyphicon glyphicon-remove"></span>&nbsp;清空</a>
									&nbsp;<span id="tip"></span>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									回复：
								</div>
								<div class="panel-body">

									<c:if test="${ requestScope.pagesize == 0 }">
										<div>暂时没有回复！</div>
									</c:if>
									<c:forEach items="${ requestScope.replylist }" var="reply">
										<div class="media">
											<div class="media-left">
												<img class="media-object img-rounded" src="${pageContext.request.contextPath}/img/${reply.user.uimage}" width="50px" height="50px">
											</div>
											<div class="media-body">
												<h5 class="media-heading">${reply.user.ualais}</h5>
												<div>${reply.rdatetime}</div>
												<p>${reply.rcontent}</p>
											</div>
										</div>
									</c:forEach>
								</div>
							</div>
							<c:if test="${ requestScope.pagesize > 1 }">
								<!--分页按钮开始-->
								<ul class='pager'>
									<li>
										<a href="${pageContext.request.contextPath}/message/info.do?mid=${ requestScope.mid }&page=1">首页</a>
									</li>
									<li>
										<a href="${pageContext.request.contextPath}/message/info.do?mid=${ requestScope.mid }&page=${ requestScope.page > 1 ? requestScope.page - 1 : 1 }">上一页</a>
									</li>
									<li>
										<a href="${pageContext.request.contextPath}/message/info.do?mid=${ requestScope.mid }&page=${ requestScope.page < requestScope.pagesize ? requestScope.page+1 : requestScope.pagesize }">下一页</a>
									</li>
									<li>
										<a href="${pageContext.request.contextPath}/message/info.do?mid=${ requestScope.mid }&page=${ requestScope.pagesize }">尾页</a>
									</li>
								</ul>
								<!--分页按钮结束-->
							</c:if>
						</div>
					</div>
					<!--内容卡片结束-->
				</div>
				<!--界面结束-->
			</div>
			<!--页面主体结束-->
			<!--页脚开始-->
				<%@ include file="../common/foot.jsp" %>
			<!--页脚结束-->
		</div>
		<!--布局容器结束-->
	</body>

</html>