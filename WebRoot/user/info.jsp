<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<title>like</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css" />
		<script src="${pageContext.request.contextPath}/js/jquery.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="${pageContext.request.contextPath}/js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="${pageContext.request.contextPath}/js/userinfo.js" type="text/javascript" charset="utf-8"></script>
	</head>
	<body>
		<!--布局容器-->
		<div class="container">
			<header class="page-header">
			</header>
			<!--导航条-->
			<nav class='navbar navbar-inverse navbar-fixed-top'>
				<div class="container">
					<div class="navbar-header">
						<a href="" class='navbar-brand'>
							Like
						</a>
						<button class='navbar-toggle collapsed' data-toggle='collapse' data-target='#mynavbar'>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>

					<div id="mynavbar" class='collapse navbar-collapse'>
						<ul class='nav navbar-nav'>
							<li>
								<a href="${pageContext.request.contextPath}/message/list.do">消息广场</a>
							</li>
							<li class='active'>
								<a href="${pageContext.request.contextPath}/user/info.do">个人中心</a>
							</li>
						</ul>

						<ul class='nav navbar-nav navbar-right'>
							<li>
								<a href="${pageContext.request.contextPath}/user/info.do">${sessionScope.ualais}</a>
							</li>
							<li>
								<a href="${pageContext.request.contextPath}/user/exit.do">退出</a>
							</li>
						</ul>
					</div>
				</div>
			</nav>
			<!--导航条结束-->
			<!--页面主体-->
			<div class="row">
				<!--左侧菜单-->
				<div class="col-md-2">
					<!--左侧菜单卡-->
					<div class="row">
						<div class="col-md-12">
							<div class="list-group">
								<a href="${pageContext.request.contextPath}/user/info.do" class="list-group-item item active">欢迎</a>
								<a href="${pageContext.request.contextPath}/user/mymessage.do" class="list-group-item">消息管理</a>
								<a href="${pageContext.request.contextPath}/user/myreply.do" class="list-group-item">回复管理</a>
							</div>
						</div>
					</div>
					<!--左侧菜单卡结束-->
				</div>
				<!--右侧界面-->
				<div class="col-md-10">
					<!--内容卡片-->
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="media">
										<div class="media-left">
											<img class="media-object img-rounded" src="${pageContext.request.contextPath}/img/${requestScope.user.uimage}" width="100px" height="100px">
										</div>
										<div class="media-body">
											<div class="media-heading"></div>
											<dl class="dl-horizontal">
												<dt>昵称</dt>
												<dd>${requestScope.user.ualais}</dd>
												<dt>性别</dt>
												<dd>${requestScope.user.usex}</dd>
												<dt>个人简介</dt>
												<dd>${requestScope.user.uinfo}</dd>
											</dl>
										</div>
										<div class="media-right">
											<button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal">
											<span class="glyphicon glyphicon-pencil"></span>&nbsp;修改
										</button>
										</div>
									</div>
								</div>
							</div>
							<!-- 修改个人资料 -->
							<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title" id="myModalLabel">修改个人资料</h4>
										</div>
										<div class="modal-body">
											<form action="" id="updatebox">
												<div class="form-group">
													<input type="text" class="form-control" id="ualais" placeholder="昵称" value="${requestScope.user.ualais}">
												</div>
												<div class="form-group">
													<input type="text" class="form-control" id="usex" placeholder="性别" value="${requestScope.user.usex}">
												</div>
												<div class="form-group">
													<input type="text" class="form-control" id="uinfo" placeholder="个人简介" value="${requestScope.user.uinfo}">
												</div>
												<div id="tip"></div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
													<button type="reset" class="btn btn-default">重置</button>
													<button type="button" class="btn btn-primary" onclick="updateuser()">保存</button>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--内容卡片结束-->
				</div>
				<!--右侧界面结束-->
			</div>
			<!--页面主体结束-->
			<!--页脚开始-->
			<%@ include file="../common/foot.jsp" %>
			<!--页脚结束-->
		</div>
		<!--布局容器结束-->
	</body>

</html>