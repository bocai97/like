<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<title>like</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/login.css" />
		<script src="${pageContext.request.contextPath}/js/jquery.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="${pageContext.request.contextPath}/js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="${pageContext.request.contextPath}/js/md5.js" type="text/javascript" charset="utf-8"></script>
		<script src="${pageContext.request.contextPath}/js/login.js" type="text/javascript" charset="utf-8"></script>
	</head>
	<body>
		<!--布局容器开始-->
		<div class="like-main-body">
			<div class="container clearfix">
				<!--logo-->
				<img class="center-block" src="${pageContext.request.contextPath}/img/likelogo.png" alt="logo" />
				<!--logo结束-->
				<h3 class="text-center">与大家一起分享你喜欢的话题</h3>
				<div class="like-tab-navs center-block">
					<!--选项卡-->
					<ul class="nav nav-tabs">
						<li role="presentation" class="tab" id="reg">
							<a href="#reg">注册</a>
						</li>
						<li role="presentation" class="tab active" id="login">
							<a href="#login">登录</a>
						</li>
					</ul>
					<!--选项卡结束-->
					<!--登录输入表单-->
					<form action="" id="loginbox">
						<div class="form-group">
							<input type="text" class="form-control" id="lb_ulogon" placeholder="账号">
						</div>
						<div class="form-group">
							<input type="password" class="form-control" id="lb_upasswd" placeholder="密码">
						</div>
						<div id="lb_tip"></div>
						<button type="button" class="btn btn-default btn-lg btn-block" onclick="login()">登录</button>
					</form>
					<!--登录输入表单结束-->
					<!--注册输入表单-->
					<form action="" id="regbox">
						<div class="form-group">
							<input type="text" class="form-control" id="rb_ulogon" placeholder="账号">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" id="rb_ualais" placeholder="呢名">
						</div>
						<div class="form-group">
							<input type="password" class="form-control" id="rb_upasswd" placeholder="密码">
						</div>
						<div class="form-group">
							<input type="password" class="form-control" id="rb_upasswd2" placeholder="确认密码">
						</div>
						<div id="rb_tip"></div>
						<button type="button" class="btn btn-default btn-lg btn-block" onclick="reg()">注册</button>
					</form>
					<!--注册输入表单结束-->
				</div>
			</div>
		</div>
		<!--布局容器结束-->
	</body>
</html>